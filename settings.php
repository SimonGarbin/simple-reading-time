<?php

function simple_reading_time_get_default_options() {

    $defaults = array(
        'reading_speed' => '250',
        'format' => array(
            'hours' => 'h',
            'minutes' => 'min',
        ),
    );

    return $defaults;
}

function simple_reading_time_get_options() {

    $default = simple_reading_time_get_default_options();
    $option = get_option('simple_reading_time_options');

    if (isset($option['reading_speed']) && $option['reading_speed']) {
        $default['reading_speed'] = $option['reading_speed'];
    }

    if (isset($option['format'])) {
        if (isset($option['format']['hours']) && $option['format']['hours']) {
            $default['format']['hours'] = $option['format']['hours'];
        }
        if (isset($option['format']['minutes']) && $option['format']['minutes']) {
            $default['format']['minutes'] = $option['format']['minutes'];
        }
    }

    return $default;
}

add_action('admin_menu', 'simple_reading_time_options_page');

function simple_reading_time_options_page() {

    add_menu_page(
            __('Simple Reading Time', 'simple-reading-time'),
            __('Simple Reading Time', 'simple-reading-time'),
            'manage_options',
            'simple_reading_time',
            'simple_reading_time_options_page_html'
    );

    remove_menu_page('simple_reading_time');
}

function simple_reading_time_options_page_html() {

    if (!current_user_can('manage_options')) {
        return;
    }
    ?>

    <h1><?php echo __('Simple Reading Time', 'simple-reading-time'); ?></h1>

    <?php settings_errors('simple_reading_time_updated_message'); ?>
    <?php settings_errors('simple_reading_time_reset_message'); ?>

    <form action="options.php" method="post">
        <?php
        settings_fields('simple_reading_time_fields');
        do_settings_sections('simple_reading_time');
        ?>
        <div class="simple-reading-time-submit-buttons-wrap">
            <?php
            submit_button(
                    __('Save changes', 'simple-reading-time'),
                    'primary',
                    'updated',
                    false
            );
            submit_button(
                    __('Reset', 'simple-reading-time'),
                    'secondary',
                    'reset',
                    false
            );
            ?>
        </div>
    </form>

    <?php
}

function simple_reading_time_sanitize_settings_cb($input) {

    if (isset($_POST['updated'])) {
        add_settings_error(
                'simple_reading_time_updated_message',
                'simple_reading_time_updated_message_code',
                __('Your settings have been saved.', 'simple-reading-time'),
                'updated'
        );
    } else if (isset($_POST['reset'])) {
        add_settings_error(
                'simple_reading_time_reset_message',
                'simple_reading_time_reset_message_code',
                __('Your settings have been reset.', 'simple-reading-time'),
                'updated'
        );
        return array();
    }

    return $input;
}

add_action('admin_init', 'simple_reading_time_settings_init');

function simple_reading_time_settings_init() {

    register_setting(
            option_group: 'simple_reading_time_fields',
            option_name: 'simple_reading_time_options',
            args: array(
                'sanitize_callback' => 'simple_reading_time_sanitize_settings_cb'
            )
    );

    add_settings_section(
            id: 'simple_reading_time_settings',
            title: null,
            callback: 'simple_reading_time_settings_cb',
            page: 'simple_reading_time'
    );

    add_settings_field(
            id: 'reading_speed',
            title: __('Reading speed:', 'simple-reading-time'),
            callback: 'simple_reading_time_field_reading_speed_cb',
            page: 'simple_reading_time',
            section: 'simple_reading_time_settings',
            args: array(
                'option_name' => 'simple_reading_time_options',
                'label_for' => 'reading_speed',
            )
    );

    add_settings_field(
            id: 'format',
            title: __('Format:', 'simple-reading-time'),
            callback: 'simple_reading_time_field_format_cb',
            page: 'simple_reading_time',
            section: 'simple_reading_time_settings',
            args: array(
                'option_name' => 'simple_reading_time_options',
                'label_for' => 'format',
            )
    );
}

function simple_reading_time_settings_cb($args) {

}

function simple_reading_time_field_reading_speed_cb($args) {

    $option_name = esc_attr($args['option_name']);
    $field_id = esc_attr($args['label_for']);

    $setting = simple_reading_time_get_options()[$field_id];
    $default = simple_reading_time_get_default_options()[$field_id];
    $value = is_numeric($setting) ? esc_attr($setting) : esc_attr($default);
    ?>

    <input type="number"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo $value; ?>"
           min="10" max="1000" step="5"
           style="width: 75px"
           autocomplete="off"
           >

    <p><?php
        esc_html_e(
                'The reading speed indicates how many words are read on '
                . 'average per minute and is used to calculate the approximate '
                . 'reading time of a post. You can overwrite this default '
                . 'value in the shortcode by passing a number to the speed '
                . 'argument.',
                'simple-reading-time'
        );
        ?></p>

    <?php
}

function simple_reading_time_field_format_cb($args) {

    $option_name = esc_attr($args['option_name']);
    $field_id = esc_attr($args['label_for']);
    $settings = simple_reading_time_get_options()[$field_id];
    ?>

    <div class="simple-reading-time-format-wrap">
        <label for="<?php echo $field_id; ?>[days]">
            <?php _e('Hours', 'simple-reading-time') ?>:
        </label>
        <input type="text"
               id="<?php echo $field_id; ?>[hours]"
               name="<?php echo $option_name; ?>[<?php echo $field_id; ?>][hours]"
               value="<?php echo esc_attr($settings['hours']); ?>"
               autocomplete="off"
               >
    </div>

    <div class="simple-reading-time-format-wrap">
        <label for="<?php echo $field_id; ?>[days]">
            <?php _e('Minutes', 'simple-reading-time') ?>:
        </label>
        <input type="text"
               id="<?php echo $field_id; ?>[minutes]"
               name="<?php echo $option_name; ?>[<?php echo $field_id; ?>][minutes]"
               value="<?php echo esc_attr($settings['minutes']); ?>"
               autocomplete="off"
               >
    </div>

    <?php
}
