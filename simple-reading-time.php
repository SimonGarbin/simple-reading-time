<?php

/**
 * Plugin Name: Simple Reading Time
 * Plugin URI: https://gitlab.com/SimonGarbin/simple-reading-time
 * Description: Display the reading time of a post.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: simple-reading-time
 * Domain Path: /languages

  Copyright 2023 Simon Garbin
  Simple Reading Time is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Simple Reading Time is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Simple Reading Time. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
defined('ABSPATH') || exit;

include plugin_dir_path(__FILE__) . '/settings.php';

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'simple_reading_time_settings_link'
);

function simple_reading_time_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=simple_reading_time');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'simple-reading-time')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Shortcode
  ----------------------------------------
 */

add_shortcode('ReadingTime', 'simple_reading_time_shortcode');

function simple_reading_time_shortcode($atts = array(), $content = null) {

    if (!is_singular()) {
        return '';
    }

    $default_speed = simple_reading_time_get_options()['reading_speed'];
    $new_atts = shortcode_atts(
            array(
                'subpages' => '',
                'speed' => $default_speed,
            ),
            $atts,
            'ReadingTime'
    );

    $speed = $default_speed;
    if (is_numeric($new_atts['speed'])) {
        if (10 <= $new_atts['speed'] && $new_atts['speed'] <= 1000) {
            $speed = (int) $new_atts['speed'];
        }
    }

    $subpages = false;
    if ((get_post_type() == 'page') && ($new_atts['subpages'] == 'true')) {
        $subpages = true;
    }

    $id = get_the_ID();
    if (!$id) {
        return '';
    }

    $output = sprintf(
            '<p class="reading-time">%1$s %2$s</p>',
            $content ? esc_html($content) . ' ' : '',
            get_the_simple_reading_time($id, $speed, $subpages)
    );

    return $output;
}

/*
  ----------------------------------------
  Reading time calculation
  ----------------------------------------
 */

if (!function_exists('get_the_simple_reading_time')) {

    /**
     * Returns the formatted reading time of a post.
     *
     * @since Simple Reading Time 1.0
     * @param int   $id             ID of the post.
     * @param int   $reading_speed  The reading speed as words per minute.
     * @param bool  $add_subpages   Whether to add the reading time of child pages.
     *
     * @return void
     */
    function get_the_simple_reading_time($id, $reading_speed = 250, $add_subpages = '') {

        $reading_time = simple_reading_time_single($id, $reading_speed);

        if ($add_subpages) {
            $subpages = get_children(
                    array(
                        'post_parent' => $id,
                        'post_status' => 'publish',
                        'orderby' => 'title'
                    )
            );

            foreach ($subpages as $subpage) {
                $reading_time += simple_reading_time_single(
                        $subpage->ID,
                        $reading_speed
                );
            }
        }

        $reading_time = simple_reading_time_formatted($reading_time);

        return $reading_time;
    }

}

if (!function_exists('simple_reading_time_single')) {

    /**
     * Calculates the reading time (in minutes) of a single post.
     *
     * @since Simple Reading Time 1.0
     * @param int   $id             ID of the post.
     * @param int   $reading_speed  Words per minute.
     *
     * @return int
     */
    function simple_reading_time_single($id, $reading_speed) {

        $content = get_the_content(post: $id);
        $content = wp_strip_all_tags($content);

        $num_words = sizeof(explode(' ', $content));
        $minutes = (int) round($num_words / $reading_speed);

        return $minutes;
    }

}

if (!function_exists('simple_reading_time_formatted')) {

    /**
     * Formats the reading time.
     *
     * @since Simple Reading Time 1.0
     * @param int   $reading_time   Reading time in minutes.
     *
     * @return string
     */
    function simple_reading_time_formatted($reading_time) {

        $format = simple_reading_time_get_options()['format'];

        $hours = (int) floor($reading_time / 60);

        if ($hours) {
            $str_hours = sprintf(
                    '%s %s ',
                    strval($hours),
                    $format['hours']
            );
            $minutes = (int) round($reading_time % 60);
        } else {
            $str_hours = '';
            $minutes = (int) round($reading_time);
        }

        if ($minutes) {
            $str_minutes = sprintf(
                    '%s %s',
                    strval($minutes),
                    $format['minutes']
            );
        } else {
            $str_minutes = '< 1 ' . $format['minutes'];
        }

        return esc_html($str_hours . $str_minutes);
    }

}

/*
  ----------------------------------------
  Settings stylesheet
  ----------------------------------------
 */

add_action('admin_enqueue_scripts', 'simple_reading_time_settings_style');

function simple_reading_time_settings_style() {

    wp_register_style(
            'simple-reading-time-settings-style',
            plugins_url('assets/css/settings.css', __FILE__),
            array(),
            false,
            'all'
    );

    wp_enqueue_style('simple-reading-time-settings-style');
}

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

function simple_reading_time_translation_dummy() {

    $plugin_description = __(
            'Display the reading time of a post.',
            'simple-reading-time'
    );
}

add_action('init', 'simple_reading_time_load_textdomain');

function simple_reading_time_load_textdomain() {

    load_plugin_textdomain(
            'simple-reading-time',
            false,
            dirname(plugin_basename(__FILE__)) . '/languages'
    );
}