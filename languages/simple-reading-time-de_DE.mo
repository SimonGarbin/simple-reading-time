��          �      �       0  #   1     U     ]     c     k     z     �     �     �  �   �     �     �  �  �  "   �     �     �     �               )     ?     M  /  _  *   �  '   �                         
          	                       Display the reading time of a post. Format: Hours Minutes Reading speed: Reset Save changes Settings Simple Reading Time The reading speed indicates how many words are read on average per minute and is used to calculate the approximate reading time of a post. You can overwrite this default value in the shortcode by passing a number to the speed argument. Your settings have been reset. Your settings have been saved. Project-Id-Version: Simple Reading Time
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-27 10:25+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-SearchPath-0: simple-reading-time.php
X-Poedit-SearchPath-1: settings.php
 Zeige die Lesezeit eines Posts an. Format: Stunden Minuten Lesegeschwindigkeit: Zurücksetzen Änderungen speichern Einstellungen Einfache Lesezeit Die Lesegeschwindigkeit gibt an, wie viele Wörter pro Minute im Durchschnitt gelesen werden, und wird verwendet, um die Lesezeit eines Posts zu berechnen. Diesen Wert kannst du überschreiben, indem du im Shortcode die Lesegeschwindigkeit mit dem speed-Argument und einer dazugehörigen Zahl festlegst. Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. 