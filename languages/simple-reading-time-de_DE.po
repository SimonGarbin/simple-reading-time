msgid ""
msgstr ""
"Project-Id-Version: Simple Reading Time\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-27 10:24+0200\n"
"PO-Revision-Date: 2023-10-27 10:25+0200\n"
"Last-Translator: \n"
"Language-Team: Simon Garbin\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e\n"
"X-Poedit-SearchPath-0: simple-reading-time.php\n"
"X-Poedit-SearchPath-1: settings.php\n"

#: settings.php:42 settings.php:43 settings.php:59
msgid "Simple Reading Time"
msgstr "Einfache Lesezeit"

#: settings.php:72
msgid "Save changes"
msgstr "Änderungen speichern"

#: settings.php:78
msgid "Reset"
msgstr "Zurücksetzen"

#: settings.php:96
msgid "Your settings have been saved."
msgstr "Deine Einstellungen wurden gespeichert."

#: settings.php:103
msgid "Your settings have been reset."
msgstr "Deine Einstellungen wurden zurückgesetzt."

#: settings.php:133
msgid "Reading speed:"
msgstr "Lesegeschwindigkeit:"

#: settings.php:145
msgid "Format:"
msgstr "Format:"

#: settings.php:181
msgid ""
"The reading speed indicates how many words are read on average per minute "
"and is used to calculate the approximate reading time of a post. You can "
"overwrite this default value in the shortcode by passing a number to the "
"speed argument."
msgstr ""
"Die Lesegeschwindigkeit gibt an, wie viele Wörter pro Minute im Durchschnitt "
"gelesen werden, und wird verwendet, um die Lesezeit eines Posts zu "
"berechnen. Diesen Wert kannst du überschreiben, indem du im Shortcode die "
"Lesegeschwindigkeit mit dem speed-Argument und einer dazugehörigen Zahl "
"festlegst."

#: settings.php:202
msgid "Hours"
msgstr "Stunden"

#: settings.php:214
msgid "Minutes"
msgstr "Minuten"

#: simple-reading-time.php:43
msgid "Settings"
msgstr "Einstellungen"

#: simple-reading-time.php:222
msgid "Display the reading time of a post."
msgstr "Zeige die Lesezeit eines Posts an."

#~ msgid "Reading time"
#~ msgstr "Lesezeit"

#~ msgid "approx."
#~ msgstr "ca."

#~ msgid "Display:"
#~ msgstr "Anzeigen:"

#~ msgid "Add subpages:"
#~ msgstr "Unterseiten hinzufügen:"

#~ msgid "Antique Reading Time"
#~ msgstr "Antique Lesezeit"

#~ msgid "Antique Plugins"
#~ msgstr "Antique Plugins"

#~ msgid ""
#~ "This plugin allows you to display the reading time of a page. When you "
#~ "create or edit pages, there is a \"Reading Time\" section in the right "
#~ "settings sidebar. Display the reading time of the page you are editing by "
#~ "activating \"Display\". If you also activate \"Add subpages\", the "
#~ "reading time is increased by the reading times of all direct subpages."
#~ msgstr ""
#~ "Mit diesem Plugin kannst du auf deinen Seiten die Lesezeit anzeigen. Wenn "
#~ "du eine Seite erstellst oder bearbeitest, siehst auf der rechten "
#~ "Seitenleiste unter den Einstellungen den Abschnitt \"Lesezeit\". Zeige "
#~ "die Lesezeit an, indem du \"Anzeigen\" aktivierst. Willst du die Lesezeit "
#~ "aller direkten Unterseiten dazurechnen, aktiviere \"Unterseiten "
#~ "hinzufügen\"."

#~ msgid ""
#~ "If you want to display the reading time at an arbitrary position within "
#~ "the page content, select the \"shortcode\" block and insert the shortcode "
#~ "[AntiqueReadingTime]. The reading speed is set with speed=N, where N "
#~ "stands for an integer between 10 and 1000, and by adding subpages=\"on\" "
#~ "the reading time of the subpages is also taken into account. Example:"
#~ msgstr ""
#~ "Mit dem Shortcode [AntiqueReadingTime] kannst du die Lesezeit an einer "
#~ "beliebigen Stelle im Seiteninhalt anzeigen. Wähle dafür den \"Shortcode\"-"
#~ "Block und füge dort den Shortcode ein. Mit speed=N, wobei N für eine "
#~ "natürliche Zahl zwischen 10 und 1000 steht, kannst du die "
#~ "Lesegeschwindigkeit festlegen und mit subpages=\"on\" die Lesezeit der "
#~ "Unterseiten dazurechnen. Beispiel:"

#~ msgid "General"
#~ msgstr "Allgemein"

#~ msgid "Position"
#~ msgstr "Position"

#~ msgid "Position:"
#~ msgstr "Position:"

#~ msgid "Block Title"
#~ msgstr "Block-Titel"

#~ msgid "Font color:"
#~ msgstr "Schriftfarbe:"

#~ msgid "Background color:"
#~ msgstr "Hintergrundfarbe:"

#~ msgid "Border:"
#~ msgstr "Rahmen:"

#~ msgid "Block Content"
#~ msgstr "Block-Inhalt"

#~ msgid "Behaviour"
#~ msgstr "Verhalten"

#~ msgid "Large screen:"
#~ msgstr "Großer Bildschirm:"

#~ msgid "Small screen:"
#~ msgstr "Kleiner Bildschirm:"

#~ msgid "words/minute"
#~ msgstr "Wörter/Minute"

#~ msgid ""
#~ "Specify the position where the reading time is displayed. This option "
#~ "does not apply to shortcodes."
#~ msgstr ""
#~ "Lege die Stelle fest, an der die Lesezeit angezeigt wird. Diese "
#~ "Einstellung hat keine Wirkung auf Shortcodes."

#~ msgid "before"
#~ msgstr "vor"

#~ msgid "after"
#~ msgstr "nach"

#~ msgid ""
#~ "Display the reading time at the top of the page content. This "
#~ "functionality is available only if you are using the Antique theme. "
#~ "Otherwise the reading time is displayed at the position specified above."
#~ msgstr ""
#~ "Zeige die Lesezeit am Anfang des Seiteninhalts an. Diese Einstellung "
#~ "steht dir nur zur Verfügung, wenn das Antique-Theme installiert und "
#~ "aktiviert ist, anderenfalls wird die Lesezeit an der oben festgelegten "
#~ "Stelle angezeigt."

#~ msgid ""
#~ "Apply the colors set within the Antique theme. This option only works if "
#~ "the Antique theme is installed and activated. Upon activation of another "
#~ "theme the colors specified above are used."
#~ msgstr ""
#~ "Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese "
#~ "Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert "
#~ "und aktiviert ist. Sobald du ein anderes Theme aktivierst, werden die "
#~ "oben festgelegten Farben verwendet."

#~ msgid "open"
#~ msgstr "offen"

#~ msgid "closed"
#~ msgstr "zu"

#~ msgid "page"
#~ msgstr "Seite"

#~ msgid "post"
#~ msgstr "Post"

#~ msgid "content, sidebar"
#~ msgstr "Inhalt, Seitenleiste"

#~ msgid "content"
#~ msgstr "Inhalt"

#~ msgid "sidebar"
#~ msgstr "Seitenleiste"

#~ msgid "Pages and posts where the reading time is displayed:"
#~ msgstr "Seiten und Posts, auf denen die Lesezeit angezeigt wird:"

#~ msgid "Show"
#~ msgstr "Anzeigen"

#~ msgid "Hide"
#~ msgstr "Verstecken"

#~ msgid "ID"
#~ msgstr "ID"

#~ msgid "type"
#~ msgstr "Typ"

#~ msgid "title"
#~ msgstr "Titel"

#~ msgid "location"
#~ msgstr "Stelle"

#~ msgid "link"
#~ msgstr "Link"

#~ msgid "Add the reading time of the subpages"
#~ msgstr "Unterseiten hinzufügen"

#~ msgid "Reading time: approx. "
#~ msgstr "Lesezeit: ca. "

#~ msgid "Example:"
#~ msgstr "Beispiel:"

#~ msgid "or"
#~ msgstr "oder"
