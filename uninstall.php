<?php

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

delete_option('simple_reading_time_options');
